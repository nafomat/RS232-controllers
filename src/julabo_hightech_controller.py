import serial
from enum import Enum
from dataclasses import dataclass
import time
from typing import Optional

class Handshake(Enum):
    HARDWARE = 2
    SOFTWARE = 1
    NONE = 0
    
class Parity(Enum):
    EVEN = serial.PARITY_EVEN
    ODD = serial.PARITY_ODD
    NONE = serial.PARITY_NONE



class JulaboHightech():
    TIME_WAIT = 0.1
    def __init__(self, serial_port: str, handshake: Handshake=Handshake.HARDWARE,
                 baudrate: int=4800, parity: Parity=Parity.EVEN):
        extra_opt = {}
        if handshake == Handshake.HARDWARE:
            extra_opt["rtscts"] = True
        elif handshake == Handshake.SOFTWARE:
            extra_opt["xonxoff"] = True
        extra_opt["parity"] = parity.value
        
        self.serial = serial.Serial(serial_port, baudrate=baudrate,
                                    bytesize=serial.SEVENBITS, **extra_opt)
        
        
        # Set temperature reference to first registry
        self._send_command("out_mode_01", "0")
        
    
    def _send_command(self, command: str, parameter: Optional[str]=None) -> str:
        if parameter is not None:
            command = f"{command} {parameter}"
        command += "\r"
            
        self.serial.write(command.encode("utf-8"))
        
        time.sleep(self.TIME_WAIT)
        out = self.serial.read_all().decode("utf-8").strip()
        return out
    
    @property
    def turned_on(self) -> bool:
        val = self._send_command("in_mode_01")
        return bool(int(val))
    @turned_on.setter
    def turned_on(self, value: bool):
        self._send_command("out_mode_01", f"{value:d}")
       
    @property
    def current_temperature(self) -> float:
        val = self._send_command("in_pv_00")
        return float(val) 
    @property
    def heating_power(self) -> int:
        val = self._send_command("in_pv_01")
        return int(val)
    @property
    def safety_sensor_temperature(self) -> float:
        val = self._send_command("in_pv_03")
        return float(val)
    @property
    def safety_sensor_threshold(self) -> float:
        val = self._send_command("in_pv_04")
        return float(val)

    @property
    def target_temperature(self) -> float:
        temp = self._send_command("in_sp_00")
        return float(temp)
    @target_temperature.setter
    def target_temperature(self, value):
        self._send_command("out_sp_00", f"{value:.2f}")
    
    @property
    def Te(self) -> int:
        val = self._send_command("in_par_01")
        return int(val)
    @property
    def Si(self) -> int:
        val = self._send_command("in_par_02")
        return int(val)
    @property
    def Ti(self) -> int:
        val = self._send_command("in_par_03")
        return int(val)
    @property
    def Xp(self) -> int:
        val = self._send_command("in_par_06")
        return int(val)
    @Xp.setter
    def Xp(self, value:int):
        self._send_command("out_par_06", f"{value:d}")
    @property
    def Tn(self) -> int:
        val = self._send_command("in_par_07")
        return int(val)
    @Tn.setter
    def Tn(self, value:int):
        self._send_command("out_par_07", f"{value:d}")
    @property
    def Tv(self) -> int:
        val = self._send_command("in_par_08")
        return int(val)
    @Tv.setter
    def Tv(self, value:int):
        self._send_command("out_par_08", f"{value:d}")
        
        
if __name__ == "__main__":
    PORT = "COM3"
    
    # We create the manager for the thermostat
    julabo = JulaboHightech(PORT)
    
    # We can set the target temperature
    julabo.target_temperature = 30.5
    
    print("We can get the internal parameter of the julabo")
    print(f"Te = {julabo.Te}")
    print(f"Si = {julabo.Si}")
    print(f"Ti = {julabo.Ti}")
    print(f"Xp = {julabo.Xp}")
    print(f"Tn = {julabo.Tn}")
    print(f"Tv = {julabo.Tv}")
    
    EXAMPLE_WAIT = 15  # This wait time is just for show
    print(f"The julabo will start working at {julabo.target_temperature} in {EXAMPLE_WAIT}s")
    time.sleep(EXAMPLE_WAIT)
    
    # We turn on the julabo
    julabo.turned_on = True
    print("Julabo turned on")
    
    # We can monitor now the temperature and heating power of the thermostat
    # until the temperature has the desired value
    
    print("Current temperature |  Heating Power")
    current_temperature = julabo.current_temperature
    target_temperature = julabo.target_temperature
    while  current_temperature!= target_temperature:
        power = julabo.heating_power
        print(f"{current_temperature}  |  {power}")
        time.sleep(30)
        current_temperature = julabo.current_temperature
    print("Target temperature reached")
        
        
    
    
    
    
    
    
    
    
        
        
        
        
    

        
    
    